#!/bin/env python3
from flask import json
from flask import Flask
from flask import Response
from flask import request


last_id = 0
todo_list = {}

app = Flask(__name__)


@app.route('/todo', methods=['GET'])
def list():
    api_todo_list = [get_item(id) for id in todo_list]

    return json_response(api_todo_list)


@app.route('/todo', methods=['POST'])
def add():
    global last_id, todo_list

    todo_item = json.loads(request.data)
    if 'label' not in todo_item:
        return json_response('Expected "label"', status=400)

    last_id += 1
    todo_list[last_id] = {
        'label': todo_item['label'],
        'done': False,
    }
    return json_response(get_item(last_id))


@app.route('/')
def hello():
    return 'Hello World!'


def get_item(id):
    return {
        'id': id,
        'label': todo_list[id]['label'],
        'done': todo_list[id]['done']
    }


def json_response(content, status=200):
    return Response(json.dumps(content),
                    mimetype='application/json',
                    status=status)


if __name__ == '__main__':
    app.run(debug=True)
