#!/bin/env python3
import unittest
from flask import json

import pytodo


class FlaskrTestCase(unittest.TestCase):

    def setUp(self):
        self.app = pytodo.app.test_client()

    def tearDown(self):
        pass

    def test_empty_list(self):
        rv = self.app.get('/todo')
        assert 0 == len(json.loads(rv.data))


if __name__ == '__main__':
    unittest.main()
